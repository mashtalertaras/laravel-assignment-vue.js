import Vue from 'vue'
import App from './App.vue'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// filters

import { date } from './helpers/filters.js'
Vue.filter('date', date)

import CommentsList from './components/CommentsList.vue'
Vue.component('CommentsList', CommentsList)

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')
