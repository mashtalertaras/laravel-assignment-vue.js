import moment from 'moment'
import * as momentTimezone from 'moment-timezone'

export const date = (value) => {
    const tz = momentTimezone.tz.guess()
    const defFormat = 'DD-MM-YYYY hh:mm:ss'
    const format = 'DD.MM.YYYY hh:mm a'
    return moment.utc(value, defFormat).tz(tz).format(format)
}